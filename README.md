## Introduction

This is a simple show for Rate Limiting and Throttle of the api requests, this test made for representing code and show config cases in appsettings.json\

this project and library meant to use in a gateway api project to control the limits of apis in microservice, its a middleware that checks every request in pipelne to secure the apis in the controller.

In this i have used two packages listed below, 

# Reference links

- [AspNetCoreRateLimit Nuget Package ](https://www.nuget.org/packages/AspNetCoreRateLimit/)
- [AspNetCoreRateLimit.Redis Nuget Package ](https://www.nuget.org/packages/AspNetCoreRateLimit.Redis/)
- [Git Repository](https://github.com/stefanprodan/AspNetCoreRateLimit)
 

## Deploy

 Same as other web apis its easy to deploy in IIS, NginX and etc... 
 you have to publish and copy the files to the directory provided before

 for middle ware first thing is to you have to install nugets 
 then the startup setting must be add and pipeline middleware must be set as it shown
 after that you have to configure appsettings.json file to your needs
 and set it to every controller routs you exposed or set the general settings as it shown 
 
## Configure appsettings.json

you can use Wildcards , Regex and static Routes 

static endpoints
```
        "Endpoint": "www.test.com/api/user/get-all",
```
Regex endpoints
```
        "Endpoint": ".+",
        "Endpoint": "((post)|(put)):/api/values",
```
Wildcards endpoints
```
        "Endpoint": "*",         
        "Endpoint": "get:/api/*",
```

you can set the setting period and limits as below:

```
        "Endpoint": "*",          
        "Period": "1s",
        "Limit": 2
```
it means user can request N request in M second time period

the time periods can be:
``` 
        "Period": "1s", // as seconds
        "Period": "15m",// as minates
        "Period": "1h", // as hours
        "Period": "7d", // as days
```

 
 By Masoud.garazhian@gmail.com